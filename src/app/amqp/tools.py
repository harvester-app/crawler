import ssl
import json
import logging
import platform
from asyncio import sleep
from json import JSONDecodeError
from typing import Dict, Optional

import pika
from pika.exceptions import AMQPConnectionError

import settings as st
import app


# from enum import Enum
#  Пример валидации полей в  headers
# class Priority(Enum):
#     LOW = 1
#     NORMAL = 5
#     HIGH = 10
#
#
# class Headers(BaseModel):  # Pydantic from pydantic import BaseModel, validator
#     job_id: str
#     priority: Priority
#     task_type: Optional[str] = None
#
#     @validator("priority", pre=True)
#     def _convert_priority(self, value):
#         return Priority[value]


""" =====Structure=====
client_properties = {
    'product': PRODUCT,
    'platform': 'Python %s' % platform.python_version(),
    'capabilities': {
        'authentication_failure_close': True,
        'basic.nack': True,
        'connection.blocked': True,
        'consumer_cancel_notify': True,
        'publisher_confirms': True
    },
    'information': 'See http://pika.rtfd.org',
    'version': pika.__version__
}
"""


class BasicAMQPClient:
    """Базовые действия с подключением к RabbitMQ """

    def __init__(self):
        self.username: str = st.AMQP_USER
        self.password: str = st.AMQP_PASS
        self.host: str = st.AMQP_HOST
        self.port: Optional[int, str] = st.AMQP_PORT
        self.vhost: str = st.AMQP_VHOST
        self.protocol: str = "ampq"
        self.client_properties: Dict = {
            'product': 'Crawler',
            'platform': f'Python {platform.python_version()}',
            'information': 'See https://gitlab.com/harvester-app/crawler',
            'version': app.__version__
        }

        self.logger = logging.getLogger(__name__)
        self._init_connection_parameter()
        self._connect()

    def _connect(self):
        attempts = 0
        while True:
            try:
                self.connection = pika.BlockingConnection(self.parameters)
                self.channel = self.connection.channel()
                if self.connection.is_open:
                    self.logger.debug(f'Connecting to {self.parameters.host}:{self.parameters.port}')
                    break
            except (AMQPConnectionError, Exception) as err:
                sleep(5)  # async sleep
                attempts += 1
                if attempts == 10:
                    raise AMQPConnectionError(err)

    def _init_connection_parameter(self):
        self.credentials = pika.PlainCredentials(self.username, self.password)
        self.parameters = pika.ConnectionParameters(
            host=self.host,
            port=int(self.port),
            virtual_host=self.vhost,
            credentials=self.credentials,
            client_properties=self.client_properties,
            heartbeat=st.TG_REQUEST_TIMEOUT,  # таймаут соединения в секундах
            blocked_connection_timeout=int(st.TG_REQUEST_TIMEOUT // 2),  # таймаут для обнаружения блокировки
        )
        if self.protocol == 'amqps':
            # SSL Context for TLS configuration
            ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
            ssl_context.set_ciphers("ECDHE+AESGCM:!ECDSA")
            self.parameters.ssl_options = pika.SSLOptions(context=ssl_context)

    def check_connection(self):
        if not self.connection or self.connection.is_closed:
            self._connect()

    def close(self):
        self.channel.close()
        self.connection.close()

    def declare_queue(self, queue_name: str, exclusive: bool = False, max_priority: int = 10):
        """ Объявление очереди на сервере, если её там не было"""

        self.check_connection()
        self.logger.debug(f"Trying to declare queue({queue_name})...")
        self.channel.queue_declare(
            queue=queue_name,
            exclusive=exclusive,
            durable=True,
            arguments={"x-max-priority": max_priority},
        )

    def declare_exchange(self, exchange_name: str, exchange_type: str = 'direct'):
        """ Объявление Exchange на сервере, если его там не было"""
        self.check_connection()
        self.channel.exchange_declare(
            exchange=exchange_name,
            exchange_type=exchange_type,
        )

    def bind_queue(self, exchange_name: str, queue_name: str, routing_key: str):
        """ Определение связи exchange c очередью """
        self.check_connection()
        self.channel.queue_bind(
            exchange=exchange_name,
            queue=queue_name,
            routing_key=routing_key,
        )

    def unbind_queue(self, exchange_name: str, queue_name: str, routing_key: str):
        """ Удаление связи exchange c очередью """
        self.channel.queue_unbind(
            queue=queue_name,
            exchange=exchange_name,
            routing_key=routing_key
        )


class BasicAMQPMessageSender(BasicAMQPClient):
    """ Модель для отправки сообщений в очередь RabbitMQ """

    def encode_message(self, body: Dict, encoding_type: str = 'bytes'):
        if encoding_type == 'bytes':
            # return msgpack.packb(body)  # msgpack данные
            self.logger.debug(f'Encode transmit message {body.__str__()[:32]}...')
            try:
                return json.dumps(body.__str__(), ensure_ascii=False).encode('utf-8')
            except TypeError as err:
                self.logger.error(f' [!] Error encode received message {body.__str__()[:32]}... Ex: {err}')
        else:
            raise NotImplemented

    def send_message(self, exchange_name: str, routing_key: str, body: Dict,
                     headers: Dict):  # headers: Optional[Headers] pydantic валидация
        body = self.encode_message(body=body)
        self.channel.basic_publish(
            exchange=exchange_name,
            routing_key=routing_key,
            body=body,
            properties=pika.BasicProperties(
                app_id='crawler-app' or self.username,
                delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE,
                priority=1,
                content_type='application/json',
                headers=headers,
                reply_to=None,
            ),
        )
        self.logger.info(f"Sent message >> Exchange: {exchange_name}, Routing Key: {routing_key}, Body: {body[:128]}")


class BasicAMQPMessageReceiver(BasicAMQPClient):
    """ Модель для приёма сообщений из очереди RabbitMQ """

    def __init__(self):
        super().__init__()
        self.channel_tag = None

    def decode_message(self, body):
        if type(body) == bytes:
            # return msgpack(unpackb)(body)  # msgpack данные, из примера
            try:
                json_body = json.loads(body.decode('utf-8'))  # Проверяем возможность преобразования, или там строка
                self.logger.debug(f'Decode received message length {len(json_body)}...')
                return json_body
            except JSONDecodeError as err:
                self.logger.error(f' [!] Error decode received message {body.decode("utf-8")[:64]}... Ex: {err}')
                print(f'[!] Error decode received message {body.decode("utf-8")[:64]}... Ex: {err}')
                return body.decode('utf-8')
        else:
            raise NotImplementedError

    def get_message(self, queue_name: str, auto_ask: bool = False):
        method_frame, header_frame, body = self.channel.basic_get(
            queue=queue_name,
            # auto_ack=auto_ask,  # Автоматически помечать как полученное и удалять из очереди
        )
        if method_frame:
            self.logger.info(f"Getting message >> {method_frame}, {header_frame}, {body}")
            return method_frame, header_frame, body
        else:
            self.logger.info("GET_MESSAGE >> No message returned")
            return None

    def consume_message(self, queue, callback):
        self.check_connection()
        self.channel.basic_qos(prefetch_count=1)  # не давать обработчику более одного сообщения в единицу времени
        self.channel_tag = self.channel.basic_consume(
            queue=queue,
            on_message_callback=callback,
            # auto_ack=True  # Автоматически помечать как полученное и удалять из очереди
        )
        self.logger.debug(" [*] Waiting for messages. To exit press CTRL+C")
        try:
            self.channel.start_consuming()
        except KeyboardInterrupt:
            self.channel.stop_consuming()

    def cancel_consumer(self):
        if self.channel_tag is not None:
            self.channel.basic_cancel(self.channel_tag)
            self.channel_tag = None
        else:
            self.logger.error("Do not cancel a non-existing job")

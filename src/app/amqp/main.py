import logging
import asyncio
import functools
from asyncio import sleep

import settings as st  #
from app.tg_crawler.tools import BasicTgClient
from app.amqp.tools import BasicAMQPMessageReceiver
from pika.exceptions import AMQPConnectionError
from pyrogram.enums import ChatMembersFilter

logger = logging.getLogger(__name__)
tg_client = BasicTgClient()


def sync(func):
    """ Асинхронивание прослушивания pika """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return asyncio.get_event_loop().run_until_complete(func(*args, **kwargs))

    return wrapper


class MainConsumer(BasicAMQPMessageReceiver):
    @sync
    async def consume(self, channel, method, properties, body):
        """
        Функция получает входящие сообщения из очереди на которую подписали в вызывающей функции
        Обёрнута в асинхронный loop
        """

        async def __do_ok():
            logger.debug(f' [*] Processing incoming message complete')
            logger.debug(f' [x] Waiting timeout {st.TG_REQUEST_TIMEOUT} sec...')
            await sleep(st.TG_REQUEST_TIMEOUT)  # Небольшой перерыв, чтобы оттянуть бан ~600s

        body = self.decode_message(body=body)

        logger.info(f' [*] Received message with\n [>] Properties: {properties}\n [>] Method: [>] {method}\n Data: {body}')

        if properties.headers.get('command'):  # TODO что если перенести это в BasicTgClient / отдельный модуль?
            match properties.headers.get('command'):
                case 'get_all':
                    if await tg_client.get_all(properties.headers.get('target'), receive_properties=properties):
                        logger.info(f' [*] Getting chat info from {properties.headers.get("target")}')
                        await __do_ok()
                case 'get_chat':
                    if await tg_client.get_chat_info(properties.headers.get('target'), receive_properties=properties):
                        logger.info(f' [*] Getting chat info from {properties.headers.get("target")} success!')
                        await __do_ok()

                case 'get_posts':
                    if properties.headers.get('target'):
                        if await tg_client.get_chat_posts(properties.headers.get('target'), receive_properties=properties, limit=10):
                            logger.info(f' [*] Getting chat posts from {properties.headers.get("target")} success!')
                            await __do_ok()
                    else:
                        logger.error(f' [*] Getting chat posts...  Error: No getting target info!')
                        await tg_client.send_error(dict(func="consumer", error="No target", command=f"{properties.headers.get('command')}"))
                case 'get_members':
                    if properties.headers.get('target'):
                        user_filter = ChatMembersFilter.SEARCH
                        if properties.headers.get('filter'):
                            user_filter = properties.headers.get('filter')
                        if await tg_client.get_chat_members(target=properties.headers.get('target'), filter_user=user_filter, receive_properties=properties):
                            logger.info(f' [*] Getting chat members from {properties.headers.get("target")} success!')
                            await __do_ok()
                    else:
                        logger.error(f' [*] Getting chat members...  Error: No getting target info!')
                        await tg_client.send_error(dict(func="consumer", error="No target", command=f"{properties.headers.get('command')}"))
                case _:
                    logger.error(f' [*] Error: Unknown Command!')
                    await tg_client.send_error(dict(func="consumer", error="Unknown Command", command=f"{properties.headers.get('command')}"))

        else:
            await tg_client.send_error(dict(func="consumer", error="No command", headers=f"{properties.headers}"))
            logger.error(f' [*] Getting chat posts from {properties.headers.get("target")} Error: No getting command!')

        channel.basic_ack(delivery_tag=method.delivery_tag)  # Подтверждаем обработку и удаление из очереди


def declare_items(_worker):
    """Подготовка сервера очереди, объявляет необходимые параметры серверу"""
    logger.debug(" [*] Declaring exchange...")
    _worker.declare_exchange(exchange_name=st.CRAWLER_EXCHANGE)
    logger.debug(" [*] Declaring queues...")
    _worker.declare_queue(queue_name=st.MAIN_QUEUE_JOB)
    _worker.declare_queue(queue_name=st.MAIN_QUEUE_CHAT)
    _worker.declare_queue(queue_name=st.MAIN_QUEUE_POST)
    _worker.declare_queue(queue_name=st.MAIN_QUEUE_MEMBER)
    _worker.declare_queue(queue_name=st.MAIN_QUEUE_MESSAGE)
    _worker.declare_queue(queue_name=st.MAIN_QUEUE_LOG)
    _worker.declare_queue(queue_name=st.MAIN_QUEUE_ERROR)
    logger.debug(" [*] Binding exchange to queues...")
    _worker.bind_queue(exchange_name=st.CRAWLER_EXCHANGE, queue_name=st.MAIN_QUEUE_JOB, routing_key='job')
    _worker.bind_queue(exchange_name=st.CRAWLER_EXCHANGE, queue_name=st.MAIN_QUEUE_CHAT, routing_key='chat')
    _worker.bind_queue(exchange_name=st.CRAWLER_EXCHANGE, queue_name=st.MAIN_QUEUE_POST, routing_key='post')
    _worker.bind_queue(exchange_name=st.CRAWLER_EXCHANGE, queue_name=st.MAIN_QUEUE_MEMBER, routing_key='member')
    _worker.bind_queue(exchange_name=st.CRAWLER_EXCHANGE, queue_name=st.MAIN_QUEUE_MESSAGE, routing_key='message')
    _worker.bind_queue(exchange_name=st.CRAWLER_EXCHANGE, queue_name=st.MAIN_QUEUE_LOG, routing_key='logs')
    _worker.bind_queue(exchange_name=st.CRAWLER_EXCHANGE, queue_name=st.MAIN_QUEUE_ERROR, routing_key='error')
    print(' [*] Deployment on AMQP server complete.')
    logger.debug(" [*] Declaring complete")


def create_consumer():
    """ Функция старта работы с очередью RabbitMQ.
        Встаёт на прослушивание указанной очереди для получения команд.
    """
    try:
        worker = MainConsumer()
        declare_items(worker)
        worker.consume_message(queue=st.MAIN_QUEUE_JOB, callback=worker.consume)
    except AMQPConnectionError as err:
        logger.error(f' [!] AMQP connection false: {err}')

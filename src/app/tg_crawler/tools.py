import asyncio
import os
import uuid
import logging
from asyncio import sleep
from typing import Dict, List, Optional
from dataclasses import dataclass

import psycopg2
from pika import BasicProperties
from pyrogram import Client
from pyrogram.types import ChatMember, Chat, Message
from pyrogram.errors import FloodWait, ChatAdminRequired, RPCError, PeerIdInvalid
from pyrogram.enums import ChatMembersFilter, ChatType

import settings as st
from app.amqp.tools import BasicAMQPMessageSender

# logging.basicConfig(level=logging.INFO, format='[!] %(asctime)s %(name)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S %z')
logging.basicConfig(level=logging.INFO, format=u'%(levelname)s:     %(filename)s [LINE:%(lineno)d] [%(asctime)s]  %(message)s')
logger = logging.getLogger(__name__)


@dataclass
class AMQPMessageHeaders:
    """Типизированный кортеж с полями headers для сообщений в очередь"""

    command: str = None
    answer_message_id: str = None  # uuid > str
    body_type: str = None
    content_length: int = None
    from_job_id: str = None  # uuid > str
    # from_worker: str = None
    job_id: str = None  # uuid > str
    message_id: str = None  # uuid > str
    part: str = None
    target: int | str = None

    def asdict(self):
        'Return a new dict which maps field names to their values.'
        return self.__dict__


class BasicTgClient:
    instance_count = 0

    def __init__(self):
        self.__class__.instance_count += 1  # Подсчёт экземпляров класса

        self.logger = logging.getLogger(__name__)

        self.session = st.SESSION_STRING
        self.device_model = st.DEVICE_MODEL
        self.user_app_version = st.USER_APP_VERSION
        self.system_version = st.SYSTEM_VERSION
        self.app_name = st.APP_NAME  # Будем считать это именем экземпляра, чтобы отличать источники информации
        self.app = Client(
            name=self.app_name,
            api_id=st.API_ID,
            api_hash=st.API_HASH,
            phone_number=st.PHONE_NUMBER,
            in_memory=True,
            app_version=self.user_app_version,  # 'Telegram iOS 9.4.1 (25132)',
            device_model=self.device_model,  # 'iPhone 8',
            system_version=self.system_version,  # "iOS 13.4",
            session_string=self.session,
            no_updates=True,  # require with takeout-True
            takeout=True,  # Useful for exporting Telegram data,  are less prone to throw FloodWait exceptions
            sleep_threshold=60,  # Set a sleep threshold for flood wait exceptions globally in this client
            # plugins=st.plugins  # TODO не грузятся, разобраться

        )
        asyncio.run_coroutine_threadsafe(self.renewal_use_time(), asyncio.get_event_loop())

    @staticmethod
    async def cleaning_data(data):
        """Очистка и нормализация данных"""
        if isinstance(data, List):
            if isinstance(data[0], ChatMember):
                data = [d.__str__() for d in data]
            elif isinstance(data[0], Message):
                data = [d.__str__() for d in data]
        return data or []

    async def renewal_use_time(self):
        """ Обновление времени использования в сервисной БД
        для того, чтобы другие экземпляры не забирали используемые учётные данные сессий
        """
        while True:
            try:
                conn = psycopg2.connect(
                    host=st.DB_HOST,
                    port=st.DB_PORT,
                    database=st.DB_SERVICE_NAME,
                    user=st.DB_SERVICE_USER,
                    password=st.DB_SERVICE_PASSWORD
                )

                cur = conn.cursor()
                # Обновление даты на текущую
                cur.execute('UPDATE crawlers_authorization'
                            ' SET logged_in_at = current_timestamp'
                            f' WHERE telegram_user_id = {os.getenv("TELEGRAM_ID")}'
                            )

                cur.close()
                conn.commit()  # Применение изменений в БД
                conn.close()
                logger.info(f"Use-Time-Account in DB updated. Connection closing")
            except Exception as err:
                logger.error(f" [!] Trying updating time using in DB: {err}")
            finally:
                logger.info(f" [*] Update Use time in DB from id: {self.app.me.id}. Awaiting {st.TG_REQUEST_TIMEOUT}sec.")
                await sleep(st.TG_REQUEST_TIMEOUT-1)
                continue

    async def send_data(self, data, exchange: str = st.CRAWLER_EXCHANGE, routing_key: str = '*', headers: Dict = None,
                        receive_properties: BasicProperties = None):
        """Метод отправки сообщений в очередь"""
        clean_data = await self.cleaning_data(data)

        send_headers = AMQPMessageHeaders(
            answer_message_id=receive_properties.headers.get('message_id'),
            message_id=str(uuid.uuid4()),
            from_job_id=receive_properties.headers.get('job_id') or None,
        )

        if headers is None:
            headers = send_headers.asdict()
        else:
            send_headers.asdict().update(headers)
        _worker = BasicAMQPMessageSender()
        _worker.send_message(exchange_name=exchange, routing_key=routing_key, body=clean_data,
                             headers=headers)

    async def send_error(self, error):
        """Отправка сообщения на сервер в очередь ошибок"""

        send_headers = AMQPMessageHeaders(body_type='error_message',)
        await self.send_data(data=error, exchange=st.CRAWLER_EXCHANGE, routing_key='error',
                             headers=send_headers.asdict())

    async def __get_chat(self, target: int | str) -> Optional[Chat]:
        """ Внутренний метод проверки и получения инфо о чате """
        while True:
            try:
                logger.debug(f' [*] Starting get chat Info from > {target}')
                response = await self.app.get_chat(target)  # Get chat info
                await sleep(1)
                logger.info(f' [*] Getting chat Info from > {target}')
                return response  # Возвращаю для функций

            except FloodWait as err:
                logger.error(f' [!] FloodWait activate in get_posts!', exc_info=err)
                await sleep(err.value + 1)  # Асинхронное ожидание переданного таймаута.
                continue
            except RPCError as err:
                logger.error(f' [x] Error get chat info!', exc_info=err)
                await self.send_error(dict(func='get_chat_info', target=target, error=err))
                return

    async def get_chat_info(self, target: int | str, receive_properties) -> Optional[True]:
        """Получение информации о чате"""
        response = await self.__get_chat(target)
        if response:
            send_headers = AMQPMessageHeaders(
                body_type='chat_info',
                target=target,
            )
            logger.debug(f' [*] Send to broker getting chat Info from > {target}')
            await self.send_data(routing_key='chat', data=response or '', headers=send_headers.asdict(),
                                 receive_properties=receive_properties)
            return True
        else:
            return

    async def get_chat_posts(self, target: int | str, receive_properties, offset: int = 0, limit: int = 500) -> Optional[True]:
        """ Функция получения сообщений группы
                Принимает:
                — Идентификатор чата [id | nickname],
                — Смещение, с которого начнётся считывание,
                — Размер порции сообщений,
                — опционально: id последнего сообщения last_id=1. В разработке...
            """

        # получаем объект чата
        chat = await self.__get_chat(target)
        if chat:
            _posts = list()
            _page = 0  # счётчик _chunk-оф

            while True:  # TODO разделить для  Каналов и Групп
                try:
                    logger.debug(f' [*] Starting get chat history messages in > {target}')
                    _chunk = self.app.get_chat_history(chat.id, limit=limit, offset=offset)
                except FloodWait as err:
                    logger.error(f' [!] FloodWait activate in get_posts: {err}')
                    await sleep(err.value + 5)  # Асинхронное ожидание переданного таймаута.
                    continue
                except Exception as err:
                    logger.error(f' [!] Error getting posts: {err}')
                    await self.send_error(dict(func='get_chat', error=err))

                async for post in _chunk:  # Достаём из генератора сообщения
                    if post.id == 1:  # last_id:
                        _posts.append(post)
                        break
                    _posts.append(post)
                _page += 1

                send_headers = AMQPMessageHeaders(
                    body_type='message',
                    content_length=len(_posts),
                    target=target,
                    part=str(_page),
                )
                logger.debug(f' [*] Sending to broker getting chat history messages in > {target}')
                await self.send_data(routing_key='message', data=_posts, headers=send_headers.asdict(),
                                     receive_properties=receive_properties)  # Отправка порции
                if not _posts or _posts[-1].id == 1:  # Проверка конца:
                    logger.debug(f' [*] END get chat history messages in > {target}. Sending {_page} parts')
                    break
                offset += len(_posts)
                _posts.clear()
            return True
        else:
            return

    async def get_chat_members(self, target: [int, str], receive_properties, limit: int = 0,
                               filter_user: ChatMembersFilter = ChatMembersFilter.SEARCH) -> Optional[True]:
        """ Получение информации о пользователях чата """
        # получаем объект чата
        # получаем объект чата
        chat = await self.__get_chat(target)
        if chat:
            # Вступаем в группу
            await self.app.join_chat(chat.id)

            _members = list()
            # Достаём из генератора сообщения
            logger.debug(f' [*] Starting get members in > {target}')
            # TODO Переделать на функцию с пагинацией
            async for _user in self.app.get_chat_members(chat.id, limit=limit, filter=filter_user):
                try:
                    if not _user.user.is_deleted:  # фильтруем удалённых пользователей
                        _members.append(_user)

                except FloodWait as err:
                    logger.error(f' [!] FloodWait activate in get_users: wait {err.value} sec.', exc_info=err)
                    await sleep(err.value + 1)  # async sleep
                    continue
                except ChatAdminRequired as err:
                    logger.error(f' [!] Error in "{target}" Admin Required!', exc_info=err)
                    await self.send_error(dict(func='get_members', target=target, error=err))
                    break

                except Exception as err:
                    logger.error(f' [!] Error getting members!', exc_info=err)
                    await self.send_error(dict(func='get_members', error=err))

            send_headers = AMQPMessageHeaders(
                body_type='members',
                target=chat.username,
                content_length=len(_members),
            )
            logger.debug(f' [*] Send to broker getting chat members in > {target}')
            await self.send_data(_members, exchange=st.CRAWLER_EXCHANGE, routing_key='member',
                                 headers=send_headers.asdict(), receive_properties=receive_properties)
            _members.clear()
            # Выходим из чата
            await self.app.leave_chat(chat.id, delete=True)
            return True
        else:
            return

    async def get_all(self, target: int | str, receive_properties) -> Optional[True]:
        """Сбор полной информации из цели"""
        target_info = await self.__get_chat(target)
        if target_info:
            await self.get_chat_info(target, receive_properties=receive_properties)
            if target_info.type != ChatType.CHANNEL:
                await self.get_chat_posts(target=target_info.id, receive_properties=receive_properties)
                await self.get_chat_members(target=target_info.id, receive_properties=receive_properties)
            else:
                try:
                    await self.app.join_chat(target_info.id)
                except RPCError as err:
                    logger.error(f' [!] Error joining in "{target}": {err}')
                await self.get_chat_posts(target=target_info.id, limit=0, receive_properties=receive_properties)
                await self.get_chat_members(target=target_info.id, receive_properties=receive_properties)
                try:
                    await self.app.leave_chat(target_info.id, delete=True)
                except RPCError as err:
                    logger.error(f' [!] Error joining in "{target}": {err}')

            logger.debug(f' [!] Getting all data from {target} complete!')
            return True
        else:
            logger.error(f' [!] Error getting info from "{target}": Get_all Fault')
            await self.send_error(dict(func='get_all', target=target, error='No getting chat info'))
            return

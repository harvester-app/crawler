# from src.app.amqp.main import tg_client, create_consumer
from app.amqp.main import tg_client, create_consumer


def main():
    tg_client.app.start()
    create_consumer()

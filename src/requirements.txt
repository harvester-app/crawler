Pyrogram~=2.0.99
TGcrypto~=1.2.5
# uvloop~=0.17.0
psycopg2-binary~=2.9.5
python-dotenv~=1.0.0
pika~=1.3.1

import logging

# from src.app.main import main
from app.main import main


# _log_format = "%(asctime)s - [%(levelname)s] - %(name)s - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s"
_log_format = u'%(levelname)s:     %(filename)s [LINE:%(lineno)d] [%(asctime)s]  %(message)s'
logging.basicConfig(level=logging.DEBUG, format=_log_format)  #, datefmt='%Y-%m-%d %H:%M:%S %z')


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.propagate = False
    logger.info(f' [*] App Running...')
    main()

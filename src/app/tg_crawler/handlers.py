import asyncio
import logging

from pyrogram import Client, filters, enums
from pyrogram.types import Message


logger = logging.getLogger(__name__)

admin_id = 61999156  # 'Ejik87'


@Client.on_message(filters.text & filters.private)
async def new_private_message(client: Client, message: Message):
    """Автоответчик с имитацией действий человека, уведомление админа о входящем сообщении"""
    print(f'{message.date}:[New message] from @{message.from_user.username}: {message.text}')
    logger.info(f' [!] Received message from {message.from_user.first_name or message.from_user.username}: '
                f'{message.text}')
    await message.forward(admin_id)  # Пересылка входящего сообщения админу
    await asyncio.sleep(3)
    await client.read_chat_history(message.chat.id)  # Прочли сообщение
    await asyncio.sleep(3)
    await client.send_chat_action(message.chat.id, enums.ChatAction.TYPING)  # Оправили статус "печатает"
    await asyncio.sleep(4)
    await client.send_message(message.from_user.id, 'Вы ошиблись...')


@Client.on_message(filters.command(["give"]))
async def test_command(client, message: Message):
    await get_tg_history(client)


async def get_tg_history(client):
    tg_chat = Client.get_chat_history(client, 777000)
    async for msg in tg_chat:
        print(msg.text)


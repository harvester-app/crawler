import os
import logging
import time

import psycopg2
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(), verbose=True)  # Load .env variables

DB_HOST = os.getenv('DB_HOST', default='localhost')
DB_PORT = int(os.getenv('DB_PORT', default=5432))
DB_SERVICE_NAME = os.getenv('DB_SERVICE_NAME', default='postgres')
DB_SERVICE_USER = os.getenv('DB_SERVICE_USER', default='postgres')
DB_SERVICE_PASSWORD = os.getenv('DB_SERVICE_PASSWORD', default='postgres')

RETRY_TIMEOUT = int(os.getenv('TG_REQUEST_TIMEOUT', default=60))


def get_worker():
    response = None
    attempt = 3
    conn = psycopg2.connect(
        host=DB_HOST,
        port=DB_PORT,
        database=DB_SERVICE_NAME,
        user=DB_SERVICE_USER,
        password=DB_SERVICE_PASSWORD
    )

    cur = conn.cursor()
    # Пытаемся подождать и снова взять данные
    while attempt:
        cur.execute('SELECT client_id, client_sha, "authorization",  telegram_user_id, phone_number, app_version, device_model, system_version, logged_in_at '
                    'FROM client_auth_view '
                    f"WHERE logged_in_at < current_timestamp - interval '{RETRY_TIMEOUT} sec'"  # Те записи, где разница более 11 минут
                    # ' LIMIT 1'
                    )

        response = cur.fetchone()
        if response:
            break
        else:
            attempt -= 1
            logging.error(f' [!] No free Client Connections data in DB! Attempt remains: {attempt}, Wait {RETRY_TIMEOUT}')
            time.sleep(RETRY_TIMEOUT+1)
    if response:
        result = dict(zip(('client_id', 'client_sha', 'session_string', 'telegram_user_id', 'phone_number', 'app_version', 'device_model', 'system_version', 'logged_in_at'), response))
        os.environ['API_ID'] = str(result.get('client_id'))
        os.environ['API_HASH'] = str(result.get('client_sha'))
        os.environ['TELEGRAM_ID'] = str(result.get('telegram_user_id'))
        os.environ['PHONE_NUMBER'] = str(result.get('phone_number'))
        os.environ['SESSION_STRING'] = str(result.get('session_string'))
        os.environ['DEVICE_MODEL'] = str(result.get('device_model'))
        os.environ['USER_APP_VERSION'] = str(result.get('app_version'))
        os.environ['SYSTEM_VERSION'] = str(result.get('system_version'))
    else:
        logging.error(' [!] No free authenticate data for Client Connections in DB!')
        exit(1)
    # Запись в базу время использования учётки
    # cur.execute('UPDATE crawlers_authorization'
    #             ' SET logged_in_at = current_timestamp'
    #             f' WHERE telegram_user_id = {result.get("telegram_user_id")}')  # Обновление даты на текущую

    cur.close()
    # conn.commit()
    conn.close()
    return result


if __name__ == '__main__':
    get_worker()

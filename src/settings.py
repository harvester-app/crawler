import os
from dotenv import load_dotenv, find_dotenv
from app.get_worker import get_worker

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

plugins = dict(  # Подключение модулей tg-handlers
        root="app",
        include=[
            'tg_crawler.handlers',
        ],
    )

load_dotenv(find_dotenv(), verbose=True)  # Load .env variables

# API_PORT = env.int('API_PORT', default=8090)
# AMQ_PORT = env.int('AMQ_PORT', default=8091)
# SCHEDULE_PORT = env.int('SCHEDULE_PORT', default=8092)

WORKERS = os.getenv('WORKERS', default=1)

DEBUG = os.getenv('DEBUG', default=False)
AUTO_RELOAD = os.getenv('AUTO_RELOAD', default=False)

APP_NAME = os.getenv('APP_NAME', default='app_name')
HOST = os.getenv('HOST', default='0.0.0.0')

# Telegram client Connections
get_worker()  # Получение из базы параметров клиента

API_ID = int(os.environ.get('API_ID')) or os.getenv('API_ID', default=12345)
API_HASH = os.environ.get('API_HASH') or os.getenv('API_HASH', default='1234567890qwertyuiofdgdfg')
PHONE_NUMBER = os.environ.get('PHONE_NUMBER') or os.getenv('PHONE_NUMBER', default='79991234567')
SESSION_STRING = os.environ.get('SESSION_STRING') or os.getenv('SESSION_STRING', default=None)
DEVICE_MODEL = os.environ.get('DEVICE_MODEL') or os.getenv('DEVICE_MODEL', default="iPhone 8")
SYSTEM_VERSION = os.environ.get('SYSTEM_VERSION') or os.getenv('SYSTEM_VERSION', default="iOS 13.4")
USER_APP_VERSION = os.environ.get('USER_APP_VERSION') or os.getenv('USER_APP_VERSION', default="Telegram iOS 9.4.1 (25132)")

# DB Connections
DB_HOST = os.getenv('DB_HOST', default='localhost')
DB_PORT = int(os.getenv('DB_PORT', default=5432))
DB_NAME = os.getenv('DB_NAME', default='postgres')
DB_USER = os.getenv('DB_USER', default='postgres')
DB_PASSWORD = os.getenv('DB_PASSWORD', default='postgres')
DB_SERVICE_NAME = os.getenv('DB_SERVICE_NAME', default='postgres')
DB_SERVICE_USER = os.getenv('DB_SERVICE_USER', default='postgres')
DB_SERVICE_PASSWORD = os.getenv('DB_SERVICE_PASSWORD', default='postgres')

DB_MAX_CONNECTIONS = int(os.getenv('DB_MAX_CONNECTIONS', default=10))
DC_POOL_RECYCLE = int(os.getenv('DC_POOL_RECYCLE', default=60))

# RabbitMQ Connections
AMQP_HOST = os.getenv('AMQP_HOST', default='localhost')
AMQP_PORT = int(os.getenv('AMQP_PORT', default=5672))
AMQP_USER = os.getenv('AMQP_USER', default='guest')
AMQP_PASS = os.getenv('AMQP_PASS', default='guest')
AMQP_VHOST = os.getenv('AMQP_VHOST', default='/')

# RabbitMQ Queue
CRAWLER_EXCHANGE = os.getenv('CRAWLER_EXCHANGE', default='crawler.exchange')
MAIN_QUEUE = os.getenv('MAIN_QUEUE', default='crawler.main')
MAIN_QUEUE_JOB = os.getenv('MAIN_QUEUE_JOB', default='crawler.job')
MAIN_QUEUE_CHAT = os.getenv('MAIN_QUEUE_CHAT', default='crawler.chat')
MAIN_QUEUE_POST = os.getenv('MAIN_QUEUE_POST', default='crawler.post')
MAIN_QUEUE_MEMBER = os.getenv('MAIN_QUEUE_MEMBER', default='crawler.member')
MAIN_QUEUE_MESSAGE = os.getenv('MAIN_QUEUE_MESSAGE', default='crawler.message')
MAIN_QUEUE_LOG = os.getenv('MAIN_QUEUE_LOG', default='crawler.log')
MAIN_QUEUE_ERROR = os.getenv('MAIN_QUEUE_ERROR', default='crawler.error')

# reconnect to rabbitmq
RECONNECT_RESTART_COUNT = int(os.getenv('RECONNECT_RESTART_COUNT', default=10))
RECONNECT_SLEEP_TIME = int(os.getenv('RECONNECT_SLEEP_TIME', default=5))

# TIMEZONE_API_HOST = os.getenv('TIMEZONE_API_HOST', default='http://worldtimeapi.org/api/')

# telegram sleep time request
TG_REQUEST_TIMEOUT = int(os.getenv('TG_REQUEST_TIMEOUT', default=60))
